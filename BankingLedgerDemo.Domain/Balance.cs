﻿using System;

namespace BankingLedgerDemo.Domain
{
    public class Balance
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public Account Acccount { get; set; }

        public decimal CurrentBalance { get; set; }

        public DateTime? LastUpdatedAtUtc { get; set; }

        public DateTime CreatedAtUtc { get; set; }
    }
}
