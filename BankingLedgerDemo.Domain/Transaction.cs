﻿using System;

namespace BankingLedgerDemo.Domain
{
    public class Transaction
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public TransactionTypeEnum Type { get; set; }

        public virtual Account CreatedByAccount { get; set; }

        public int CreatedByAccountId { get; set; }

        public DateTime CreatedAtUtc { get; set; }
    }

    public enum TransactionTypeEnum
    {
        Deposit = 11,

        Withdrawal = 22
    }
}
