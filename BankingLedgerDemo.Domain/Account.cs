﻿namespace BankingLedgerDemo.Domain
{
    public class Account
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string AccountNumber { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }
    }
}
