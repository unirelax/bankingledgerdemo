﻿using System;
using System.Collections.Generic;
using System.Linq;
using BankingLedgerDemo.Services;

namespace ConsoleApp
{
    public class AppInteraction : IAppInteraction
    {
        private readonly Dictionary<ConsoleKey, (bool, Action)> handledOperations;
        private readonly IUserSession userSession;
        private readonly IAccountService accountService;
        private readonly ITransactionService transactionService;
        private readonly ILedgerService ledgerService;

        public AppInteraction(
            IUserSession userSession,
            IAccountService accountService,
            ITransactionService transactionService,
            ILedgerService ledgerService)
        {
            this.userSession = userSession;
            this.accountService = accountService;
            this.transactionService = transactionService;
            this.ledgerService = ledgerService;

            var requiredAuthenticated = true;

            handledOperations = new Dictionary<ConsoleKey, (bool, Action)>
            {
                { ConsoleKey.C, (!requiredAuthenticated, HandleCreateAccount) },
                { ConsoleKey.L, (!requiredAuthenticated, HandleLogin) },
                { ConsoleKey.D, (requiredAuthenticated, HandleAccountDeposit) },
                { ConsoleKey.W, (requiredAuthenticated, HandleAccountWithdrawal) },
                { ConsoleKey.B, (requiredAuthenticated, HandleCheckBalance) },
                { ConsoleKey.H, (requiredAuthenticated, HandleListTransactionHistory) },
                { ConsoleKey.O, (requiredAuthenticated, HandleLogout) }
            };
        }

        public void Start()
        {
            ConsoleKeyInfo consoleKeyInfo = default(ConsoleKeyInfo);

            do
            {
                PrintOptions();
                consoleKeyInfo = Console.ReadKey();
                ConsolePrint(null);

                if (!handledOperations.ContainsKey(consoleKeyInfo.Key))
                {
                    ConsolePrint("This is not a supported feature!");
                    continue;
                }

                var (requireAuthenticated, registeredHandler) = handledOperations[consoleKeyInfo.Key];

                if (requireAuthenticated != this.userSession.Authenticated)
                {
                    ConsolePrint("This is not a supported feature!");
                    continue;
                }

                registeredHandler();
            }
            while (consoleKeyInfo.Key != ConsoleKey.Escape);
        }

        private void HandleCreateAccount()
        {
            var username = ReadLine.Read(prompt: "Enter username:");
            var password = ReadLine.ReadPassword(prompt: "Enter password:");

            var result = this.accountService.CreateNewAccount(username, password);

            switch (result.Hint)
            {
                default:
                    {
                        break;
                    }

                case CreateAccountResultEnum.Succeeded:
                    {
                        this.userSession.CreateSession(result.Data.Id, result.Data.Username);
                        PrintWelcome();
                        break;
                    }

                case CreateAccountResultEnum.UsernameExisting:
                    {
                        ConsolePrint("This username has already been used!");
                        break;
                    }
            }
        }

        private void HandleLogin()
        {
            var username = ReadLine.Read(prompt: "Enter username:");
            var password = ReadLine.ReadPassword(prompt: "Enter password:");

            var result = this.accountService.Login(username, password);

            switch (result.Hint)
            {
                default:
                    {
                        break;
                    }

                case LoginResultEnum.PasswordMismatch:
                case LoginResultEnum.UserNotFound:
                    {
                        ConsolePrint("Either the input username or password is not correct!");
                        Wait();
                        break;
                    }

                case LoginResultEnum.Succeeded:
                    {
                        this.userSession.CreateSession(result.Data.Id, result.Data.Username);
                        PrintWelcome();
                        break;
                    }
            }
        }

        private void HandleAccountDeposit()
        {
            var amount = ReadLine.Read("Input deposit amount (VND):");

            if (!decimal.TryParse(amount, out decimal v))
            {
                return;
            }

            if (v <= 0)
            {
                ConsolePrint("Amount must be greater than 0!");
                return;
            }

            this.transactionService.AddDepositTransaction(this.userSession.UserId.Value, v);
            ConsolePrint($"{amount} VND has been deposited to your account.");
        }

        private void HandleAccountWithdrawal()
        {
            var amount = ReadLine.Read("Enter withdrawal amount (VND):");

            if (!decimal.TryParse(amount, out decimal v))
            {
                return;
            }

            if (v <= 0)
            {
                ConsolePrint("Amount must be greater than 0!");
                return;
            }

            var result = this.transactionService.AddWithdrawalTransaction(this.userSession.UserId.Value, v);

            switch (result.Hint)
            {
                default:
                    {
                        break;
                    }

                case TransactionResultEnum.InsufficientBalance:
                    {
                        ConsolePrint("Your current balance is not sufficient!");
                        break;
                    }

                case TransactionResultEnum.Succeeded:
                    {
                        ConsolePrint($"{v} (VND) has been withdrawn from your account.");
                        break;
                    }
            }
        }

        private void HandleCheckBalance()
        {
            var currentBalance = this.ledgerService.GetBalanceByAccountId(this.userSession.UserId.Value);

            ConsolePrint($"Current balance (VND): {currentBalance}");
        }

        private void HandleListTransactionHistory()
        {
            var transactions = this.transactionService.GetAllTransactionByAccountId(this.userSession.UserId.Value, orderByDescending: trans => trans.CreatedAtUtc);

            if (transactions == null || !transactions.Any())
            {
                ConsolePrint("No transaction found.");
                return;
            }

            foreach (var e in transactions)
            {
                ConsolePrint(string.Format($"|{e.Id,5}|VND {e.Amount,20}|{e.Type.ToString(),10}|{e.CreatedAtUtc,5} UTC|"));
            }
        }

        private void HandleLogout()
        {
            this.userSession.ClearSession();
        }

        private void PrintOptions()
        {
            if (this.userSession.Authenticated)
            {
                PrintAuthenticatedOptions();
                return;
            }

            PrintUnauthenticatedOptions();
        }

        private void PrintUnauthenticatedOptions(bool shouldWait = true)
        {
            ConsoleClear();

            ConsolePrint($"Press {ConsoleKey.C.ToString()} to create an account");
            ConsolePrint($"Press {ConsoleKey.L.ToString()} to login");

            ConsolePrint($"Your option:", newLine: false);
        }

        private void PrintAuthenticatedOptions()
        {
            ConsolePrint($"Press {ConsoleKey.D.ToString()} to make a deposit");
            ConsolePrint($"Press {ConsoleKey.W.ToString()} to make a withdrawal");
            ConsolePrint($"Press {ConsoleKey.B.ToString()} to check your balance");
            ConsolePrint($"Press {ConsoleKey.H.ToString()} to see transaction history");
            ConsolePrint($"Press {ConsoleKey.O.ToString()} to logout");

            ConsolePrint($"Your option:", newLine: false);
        }

        private void PrintWelcome()
        {
            ConsolePrint($"Welcome {this.userSession.Username}!");
        }

        private void Wait()
        {
            Console.ReadKey();
        }

        private void ConsolePrint(string text, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(text);
            }
            else
            {
                Console.Write(text);
            }
        }

        private void ConsoleClear()
        {
            Console.Clear();
            ReadLine.ClearHistory();
        }
    }
}
