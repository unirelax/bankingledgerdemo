﻿using System;

namespace ConsoleApp
{
    public class UserSession : IUserSession
    {
        public UserSession()
        {
            this.ResetProps();
        }

        public bool Authenticated { get; private set; } = false;

        public string Username { get; private set; } = null;

        public int? UserId { get; private set; } = null;

        private void ResetProps()
        {
            this.Authenticated = false;
            this.Username = null;
            this.UserId = null;
        }

        public void ClearSession()
        {
            this.ResetProps();
        }

        public void CreateSession(int userId, string username)
        {
            if (userId <= 0)
            {
                throw new ArgumentException(nameof(userId));
            }

            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException(username);
            }

            this.Authenticated = true;
            this.UserId = userId;
            this.Username = username;
        }
    }
}
