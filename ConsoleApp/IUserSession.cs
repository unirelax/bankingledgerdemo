﻿namespace ConsoleApp
{
    public interface IUserSession
    {
        void CreateSession(int userId, string username);

        void ClearSession();

        bool Authenticated { get; }

        int? UserId { get; }

        string Username { get; }
    }
}
