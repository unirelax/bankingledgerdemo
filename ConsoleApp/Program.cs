﻿using BankingLedgerDemo.Data;
using BankingLedgerDemo.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var serviceProvider = RegisterServices())
            {
                var appInteraction = serviceProvider.GetService<IAppInteraction>();
                appInteraction.Start();
            }
        }

        static ServiceProvider RegisterServices()
        {
            var serviceProvider = new ServiceCollection()
                                    .AddScoped<IUserSession, UserSession>()
                                    .AddScoped<IAccountService, AccountService>()
                                    .AddScoped<IAppInteraction, AppInteraction>()
                                    .AddScoped<ITransactionService, TransactionService>()
                                    .AddScoped<ILedgerService, LedgerService>()
                                    .AddSingleton<IDbContext, InMemoryDataStore>()
                                    .BuildServiceProvider();

            return serviceProvider;
        }
    }
}
