﻿using System;
using System.Linq;
using BankingLedgerDemo.Data;
using BankingLedgerDemo.Domain;

namespace BankingLedgerDemo.Services
{
    public class AccountService : IAccountService
    {
        private readonly IDbContext dbContext;

        public AccountService(IDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public CombinedResult<Account, CreateAccountResultEnum> CreateNewAccount(string username, string rawPassword)
        {
            var result = new CombinedResult<Account, CreateAccountResultEnum>();

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(rawPassword))
            {
                result.Hint = CreateAccountResultEnum.Error;
                return result;
            }

            if (dbContext.Accounts.Any(e => string.Equals(e.Username, username, StringComparison.OrdinalIgnoreCase)))
            {
                result.Hint = CreateAccountResultEnum.UsernameExisting;
                return result;
            }

            var salt = PasswordUtil.GenerateSalt();
            var userPasswordHash = PasswordUtil.ComputeHash(rawPassword, salt);
            var accNumber = Guid.NewGuid().ToString();

            var account = new Account
            {
                AccountNumber = accNumber,
                PasswordHash = userPasswordHash,
                PasswordSalt = salt,
                Username = username
            };

            account = this.dbContext.AddNewAccount(account);

            result.Data = account;
            result.Hint = CreateAccountResultEnum.Succeeded;

            return result;
        }

        public CombinedResult<Account, LoginResultEnum> Login(string username, string rawPassword)
        {
            var result = new CombinedResult<Account, LoginResultEnum>();

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(rawPassword))
            {
                result.Hint = LoginResultEnum.Error;
                return result;
            }

            var foundAccount = this.dbContext.Accounts.FirstOrDefault(e => string.Equals(e.Username, username, StringComparison.OrdinalIgnoreCase));

            if (foundAccount == null)
            {
                result.Hint = LoginResultEnum.UserNotFound;
                return result;
            }

            var currentPasswordHash = foundAccount.PasswordHash;
            var inputPasswordHash = PasswordUtil.ComputeHash(rawPassword, foundAccount.PasswordSalt);

            if (!string.Equals(currentPasswordHash, inputPasswordHash, StringComparison.Ordinal))
            {
                result.Hint = LoginResultEnum.PasswordMismatch;
                return result;
            }

            result.Hint = LoginResultEnum.Succeeded;
            result.Data = foundAccount;
            return result;
        }
    }
}
