﻿using BankingLedgerDemo.Domain;

namespace BankingLedgerDemo.Services
{
    public interface IAccountService
    {
        CombinedResult<Account, CreateAccountResultEnum> CreateNewAccount(string username, string rawPassword);

        CombinedResult<Account, LoginResultEnum> Login(string username, string rawPassword);
    }

    public enum CreateAccountResultEnum
    {
        Succeeded = 11,

        UsernameExisting = 22,

        Error = 99
    }

    public enum LoginResultEnum
    {
        Succeeded = 11,

        UserNotFound = 22,

        PasswordMismatch = 33,

        Error = 99,
    }
}
