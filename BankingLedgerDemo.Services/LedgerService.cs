﻿using System;
using System.Linq;
using BankingLedgerDemo.Data;

namespace BankingLedgerDemo.Services
{
    public class LedgerService : ILedgerService
    {
        private readonly IDbContext dbContext;

        public LedgerService(IDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public decimal GetBalanceByAccountId(int accountId)
        {
            var balance = this.dbContext.Balances.FirstOrDefault(e => e.AccountId == accountId);

            return Math.Round(balance?.CurrentBalance ?? 0, 3);
        }
    }
}
