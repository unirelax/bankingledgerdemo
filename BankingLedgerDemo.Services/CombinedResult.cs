﻿using System;

namespace BankingLedgerDemo.Services
{
    public class CombinedResult<TData, THint>
        where TData : class
        where THint : struct, IConvertible
    {
        public CombinedResult()
        {
            this.Data = default(TData);
            this.Hint = default(THint);
        }

        public CombinedResult(TData data, THint hint)
        {
            this.Data = data;
            this.Hint = hint;
        }

        public TData Data { get; set; }

        public THint Hint { get; set; }
    }
}
