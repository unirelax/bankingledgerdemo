﻿using System;
using System.Security.Cryptography;

namespace BankingLedgerDemo.Services
{
    internal static class PasswordUtil
    {
        // 24 = 192 bits
        private const int SaltByteSize = 24;
        private const int HashByteSize = 24;
        private const int HasingIterationsCount = 1010;

        public static string GenerateSalt()
        {
            var saltBytes = GenerateSalt(SaltByteSize);
            return Convert.ToBase64String(saltBytes);
        }

        public static string ComputeHash(string rawPassword, string salt)
        {
            var saltBytes = Convert.FromBase64String(salt);
            var computedHashBytes = ComputeHash(rawPassword, saltBytes, HasingIterationsCount, HashByteSize);
            return Convert.ToBase64String(computedHashBytes);
        }

        private static byte[] GenerateSalt(int saltByteSize)
        {
            using (var saltGenerator = new RNGCryptoServiceProvider())
            {
                var salt = new byte[saltByteSize];
                saltGenerator.GetBytes(salt);
                return salt;
            }
        }

        private static byte[] ComputeHash(string password, byte[] salt, int iterations, int hashByteSize)
        {
            using (var hashGenerator = new Rfc2898DeriveBytes(password, salt))
            {
                hashGenerator.IterationCount = iterations;
                return hashGenerator.GetBytes(hashByteSize);
            }
        }
    }
}
