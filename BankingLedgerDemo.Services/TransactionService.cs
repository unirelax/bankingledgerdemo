﻿using System;
using System.Collections.Generic;
using System.Linq;
using BankingLedgerDemo.Data;
using BankingLedgerDemo.Domain;

namespace BankingLedgerDemo.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IDbContext dbContext;
        private readonly ILedgerService ledgerService;

        public TransactionService(IDbContext dbContext, ILedgerService ledgerService)
        {
            this.dbContext = dbContext;
            this.ledgerService = ledgerService;
        }

        public Transaction AddDepositTransaction(int accountId, decimal amount)
        {
            UpdateAccountBalance(accountId, amount, TransactionTypeEnum.Deposit);
            var transaction = this.AddTransaction(accountId, amount, TransactionTypeEnum.Deposit);

            return transaction;
        }

        public CombinedResult<Transaction, TransactionResultEnum> AddWithdrawalTransaction(int accountId, decimal amount)
        {
            var result = new CombinedResult<Transaction, TransactionResultEnum>();

            var balanceUpdated = UpdateAccountBalance(accountId, amount, TransactionTypeEnum.Withdrawal);

            if (!balanceUpdated)
            {
                result.Hint = TransactionResultEnum.InsufficientBalance;
                return result;
            }

            var transaction = this.AddTransaction(accountId, amount, TransactionTypeEnum.Withdrawal);

            result.Hint = TransactionResultEnum.Succeeded;
            result.Data = transaction;

            return result;
        }

        public IEnumerable<Transaction> GetAllTransactionByAccountId(int accountId, Func<Transaction, object> orderBy = null, Func<Transaction, object> orderByDescending = null)
        {
            if (orderBy != null)
            {
                return this.dbContext.Transactions.Where(e => e.CreatedByAccountId == accountId).OrderBy(orderBy);
            }

            if (orderByDescending != null)
            {
                return this.dbContext.Transactions.Where(e => e.CreatedByAccountId == accountId).OrderByDescending(orderByDescending);
            }

            return this.dbContext.Transactions.Where(e => e.CreatedByAccountId == accountId);
        }

        private Transaction AddTransaction(int accountId, decimal amount, TransactionTypeEnum transactionTypeEnum)
        {
            if (amount < 0)
            {
                throw new ArgumentException(nameof(amount));
            }

            var transaction = new Transaction
            {
                CreatedAtUtc = DateTime.UtcNow,
                CreatedByAccountId = accountId,
                Amount = amount,
                Type = transactionTypeEnum
            };

            transaction = this.dbContext.AddNewTransaction(transaction);
            return transaction;
        }

        private bool UpdateAccountBalance(int accountId, decimal amount, TransactionTypeEnum transactionTypeEnum)
        {
            var currentBalance = this.ledgerService.GetBalanceByAccountId(accountId);

            if (currentBalance < amount && transactionTypeEnum == TransactionTypeEnum.Withdrawal)
            {
                return false;
            }

            var depositAmount = transactionTypeEnum == TransactionTypeEnum.Deposit ? amount : 0;
            var withdrawalAmount = transactionTypeEnum == TransactionTypeEnum.Withdrawal ? amount : 0;

            this.dbContext.AddOrUpdateAccountBalance(accountId, depositAmount, withdrawalAmount);
            return true;
        }
    }
}
