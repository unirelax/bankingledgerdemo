﻿using System;
using System.Collections.Generic;
using BankingLedgerDemo.Domain;

namespace BankingLedgerDemo.Services
{
    public interface ITransactionService
    {
        Transaction AddDepositTransaction(int accountId, decimal amount);

        CombinedResult<Transaction, TransactionResultEnum> AddWithdrawalTransaction(int accountId, decimal amount);

        IEnumerable<Transaction> GetAllTransactionByAccountId(int accountId, Func<Transaction, object> orderBy = null, Func<Transaction, object> orderByDescending = null);
    }

    public enum TransactionResultEnum
    {
        Succeeded = 11,

        InsufficientBalance = 22
    }
}
