﻿namespace BankingLedgerDemo.Services
{
    public interface ILedgerService
    {
        decimal GetBalanceByAccountId(int accountId);
    }
}
