﻿using System;
using System.Collections.Generic;
using System.Linq;
using BankingLedgerDemo.Domain;

namespace BankingLedgerDemo.Data
{
    public class InMemoryDataStore : IDbContext
    {
        private int currentAccountId = 0;
        private int currentTransactionId = 0;
        private int currentBalanceId = 0;

        public InMemoryDataStore()
        {
            this.Accounts = new List<Account>();
            this.Transactions = new List<Transaction>();
            this.Balances = new List<Balance>();
        }

        public IList<Account> Accounts { get; }

        public IList<Transaction> Transactions { get; }

        public IList<Balance> Balances { get; }

        public Account AddNewAccount(Account account)
        {
            currentAccountId++;
            account.Id = currentAccountId;

            this.Accounts.Add(account);

            return account;
        }

        public Transaction AddNewTransaction(Transaction transaction)
        {
            currentTransactionId++;
            transaction.Id = currentTransactionId;

            this.Transactions.Add(transaction);

            return transaction;
        }

        public Balance AddOrUpdateAccountBalance(Balance balance)
        {
            var existingRecord = this.Balances.FirstOrDefault(e => e.AccountId == balance.AccountId);

            if (existingRecord == null)
            {
                currentBalanceId++;
                balance.Id = currentBalanceId;
                balance.CreatedAtUtc = DateTime.UtcNow;

                this.Balances.Add(balance);
                return balance;
            }

            existingRecord.LastUpdatedAtUtc = DateTime.UtcNow;
            existingRecord.CurrentBalance += balance.CurrentBalance;

            return existingRecord;
        }

        public Balance AddOrUpdateAccountBalance(int accountId, decimal depositAmount = 0, decimal withdrawalAmount = 0)
        {
            var finalAmount = depositAmount - withdrawalAmount;

            var existingRecord = this.Balances.FirstOrDefault(e => e.AccountId == accountId);

            if (existingRecord == null)
            {
                currentBalanceId++;

                var balance = new Balance
                {
                    Id = currentBalanceId,
                    CreatedAtUtc = DateTime.UtcNow,
                    AccountId = accountId,
                    CurrentBalance = finalAmount
                };

                this.Balances.Add(balance);
                return balance;
            }

            existingRecord.LastUpdatedAtUtc = DateTime.UtcNow;
            existingRecord.CurrentBalance += finalAmount;

            return existingRecord;
        }
    }
}
