﻿using System.Collections.Generic;
using BankingLedgerDemo.Domain;

namespace BankingLedgerDemo.Data
{
    public interface IDbContext
    {
        Account AddNewAccount(Account account);

        Transaction AddNewTransaction(Transaction account);

        Balance AddOrUpdateAccountBalance(int accountId, decimal depositAmount = 0, decimal withdrawalAmount = 0);

        IList<Account> Accounts { get; }

        IList<Transaction> Transactions { get; }

        IList<Balance> Balances { get; }
    }
}
